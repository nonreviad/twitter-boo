const twitterBlueUsers = new Set()
const styleNode = document.createElement('style')
document.head.append(styleNode)
function parseUserResults(user_results) {
    if (!user_results) return
    const blueVerified = user_results.is_blue_verified
    const name = user_results.legacy.screen_name
    if (blueVerified) {
        twitterBlueUsers.add(name)
    } else {
        twitterBlueUsers.delete(name)
    }
}
function parseTweetResults(tweet_results) {
    if (!tweet_results) return
    const result = tweet_results.result
    const core = result.core
    if (core) {
        parseUserResults(core.user_results.result)
    }
    const legacy = result.legacy
    if (legacy && legacy.retweeted_status_result) {
        parseTweetResults(legacy.retweeted_status_result)
    }
}
function parseItemContent(itemContent) {
    if (!itemContent || itemContent.itemType !== 'TimelineTweet') {
        return
    }
    parseTweetResults(itemContent.tweet_results)   
}

function sussifyBlueUsers() {
    const lUrl = document.getElementById('bootstrap').getAttribute('extra_info')
    styleNode.textContent = [...twitterBlueUsers].map(user => `
        a[href="/${user}"] > div > div + div > svg {
            display: none;
        }
        a[href="/${user}"] > div > div + div {
            content: url(${lUrl});
            width: 1.5rem;
            height: 1.5rem;
            margin-left: 0.25rem;
        }
        a[href="/${user}/header_photo"] + div > div + div > div > div > div > div> div:first-child > span + span > div > div > svg{
            display: none;
        }
        a[href="/${user}/header_photo"] + div > div + div > div > div > div > div> div:first-child > span + span > div > div {
            content: url(${lUrl});
            width: 1.5rem;
            height: 1.5rem;
            margin-left: 0.25rem;
        }
    `).join('\n')
}

function parseContent(content) {
    if (content.entryType === "TimelineTimelineItem") {
        parseItemContent(content.itemContent)
    } else if (content.entryType === "TimelineTimelineModule") {
        for (const item of content.items) {
            parseItemContent(item.item.itemContent)
        }
    }
}

function parseInstruction(instruction) {
    if (instruction.type === "TimelineAddEntries") {
        for (const entry of instruction.entries) {
            const content = entry.content
            if (!content) continue
            parseContent(content)
        }
    }
}
class MyXMLHttpRequest extends XMLHttpRequest {
    constructor() {
        super()
    }
   
    open(method, url) {
        super.open(method, url)
        if (url.toString().includes("HomeTimeline")) {
            this.addEventListener('load', () => {
                const response = JSON.parse(this.response)
                const instructions = response.data.home.home_timeline_urt.instructions
                instructions.forEach(parseInstruction)
                sussifyBlueUsers()
            })
        } else if (url.toString().includes("TweetDetail")) {
            this.addEventListener('load', () => {
                const response = JSON.parse(this.response)
                const instructions = response.data.threaded_conversation_with_injections_v2.instructions
                instructions.forEach(parseInstruction)
                sussifyBlueUsers()
            })
        } else if (url.toString().includes("UserTweets") || url.toString().includes("Likes")) {
            this.addEventListener('load', () => {
                const response = JSON.parse(this.response)
                const instructions = response.data.user.result.timeline_v2.timeline.instructions
                instructions.forEach(parseInstruction)
                sussifyBlueUsers()
            })
        }
    }
}

window.XMLHttpRequest = MyXMLHttpRequest
globalThis.XMLHttpRequest = MyXMLHttpRequest
