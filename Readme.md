# Twitter Boo

## What is this?

Twitter Blue? More like Twitter **BOO**, amirite gamers?

This will replace checkmarks belonging to folks who paid $8 for Twitter Blue verification with a big L. Regular checkmarks are spared. Unless they also paid for Twitter Blue.

## Why?!

The idea for this came from seeing [who-blue](https://github.com/wesbos/who-blue), although my approach is worse and over-engineered.

## How do I install it?

I mean, you probably shouldn't. But here are the steps:

- Download the [repository](https://gitlab.com/nonreviad/twitter-boo/-/archive/main/twitter-boo-main.zip) as a zip archive.
- Unzip it somewhere on your computer.
- Go to `chrome://extensions/` in Chrome.
- Toggle on `Developer mode` if it isn't.
- Click `Load unpacked` and select the unzipped directory.
- Any existing Twitter tabs will need to be refreshed for it to work.
- Point and laugh at those paying for Twitter Blue.

## Is this safe?

Yeah, pinky promise!

## What about other browsers?

I mean, it's a simple fix for Firefox, but it's too much effort for a shitpost. Any other Chromium based browsers should work. Safari will not.